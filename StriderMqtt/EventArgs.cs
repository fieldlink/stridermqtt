﻿using System;

namespace StriderMqtt
{
	public class PublishReceivedEventArgs : EventArgs
	{
		/// <summary>
		/// The packet id of the received message.
		/// </summary>
		public ushort PacketId { get; private set; }

		/// <summary>
		/// A flag indicating if the message is duplicate.
		/// It is set to true when another trial was made previously.
		/// </summary>
		public bool DupFlag { get; private set; }

		/// <summary>
		/// Quality of Service of the received message.
		/// </summary>
		public MqttQos QosLevel { get; private set; }

		/// <summary>
		/// A flag indicating if the message is retained.
		/// </summary>
		public bool Retain { get; private set; }

		/// <summary>
		/// The topic of the received message.
		/// </summary>
		public string Topic { get; private set; }

		/// <summary>
		/// The Application Message received from broker.
		/// </summary>
		public byte [] Message { get; private set; }

		public PublishReceivedEventArgs (ushort packetId, bool dupFlag, MqttQos qosLevel, bool retain, string topic, byte[] message) : base ()
		{
			PacketId = packetId;
			DupFlag = dupFlag;
			QosLevel = qosLevel;
			Retain = retain;
			Topic = topic;
			Message = message;
		}
	}

    public class BatchReceivedEventArgs : EventArgs
    {
        public PublishReceivedEventArgs[] Publishes { get; private set; }

        public BatchReceivedEventArgs(PublishReceivedEventArgs[] publishes)
        {
            Publishes = publishes;
        }
    }

    public class IdentifiedPacketEventArgs : EventArgs
	{
		public ushort PacketId { get; private set; }

		public IdentifiedPacketEventArgs (ushort packetId)
		{
			PacketId = packetId;
		}
	}

	public class SubackReceivedEventArgs : EventArgs
	{
		public SubackReturnCode [] GrantedQosLevels { get; private set; }

		public SubackReceivedEventArgs (SubackReturnCode[] returnCode)
		{
			GrantedQosLevels = returnCode;
		}
	}
}
