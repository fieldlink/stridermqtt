﻿using System;

using SQLite;

namespace StriderMqtt.Sqlite.Models
{
    public class IncomingFlow
    {
        #region Properties

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int PacketId { get; set; }

        #endregion
    }
}
