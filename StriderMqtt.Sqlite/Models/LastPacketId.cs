﻿using System;

using SQLite;

namespace StriderMqtt.Sqlite.Models
{
    public class LastPacketId
    {
        #region Properties

        [Unique]
        public int Id { get; set; }

        #endregion
    }
}   
