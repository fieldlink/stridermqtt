﻿using System;

using SQLite;

namespace StriderMqtt.Sqlite.Models
{
    public class OutgoingFlow : StriderMqtt.OutgoingFlow
    {
        #region Properties

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        #endregion
    }
}
