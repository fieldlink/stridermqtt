using System;
using System.IO;
using System.Collections.Generic;

using SQLite;

namespace StriderMqtt.Sqlite
{
	public class SqlitePersistence : IMqttPersistence, IDisposable
	{
		bool ConnectionIsManaged;
		SQLiteConnection conn;

		public SqlitePersistence(string filename)
		{
			conn = new SQLiteConnection(filename);
			ConnectionIsManaged = true;
			CreateTables();
		}

		public SqlitePersistence(SQLiteConnection connection)
		{
			conn = connection;
			ConnectionIsManaged = false;
			CreateTables();
		}

		void CreateTables()
		{
			try
			{
				conn.BeginTransaction();

				// stores packet ids for messages that are arriving from broker
				conn.CreateTable<Models.IncomingFlow>();

				// stores the last used packet id
				conn.CreateTable<Models.LastPacketId>();

				// stores messages that being delivered to the broker
				conn.CreateTable<Models.OutgoingFlow>();
				conn.Commit();
			}
			catch(Exception) {
				conn.Rollback();
			}
		}

		public void RegisterIncomingFlow(ushort packetId)
		{
			var incoming = new Models.IncomingFlow();
			incoming.PacketId = packetId;
			conn.InsertOrReplace(incoming);
		}

		public void ReleaseIncomingFlow(ushort packetId)
	    {
			conn.Table<Models.IncomingFlow>().Delete(item => item.PacketId == packetId);
		}

		public bool IsIncomingFlowRegistered(ushort packetId)
		{

			var result = conn.Table<Models.IncomingFlow>().FirstOrDefault(item => item.PacketId == packetId);
			return result != null;
		}


		public ushort LastOutgoingPacketId
		{
			get
			{
				var item = conn.Table<Models.IncomingFlow>().FirstOrDefault();
				return item == null ? default(ushort) : (ushort)item.PacketId;
			}

			set
			{
				RegisterIncomingFlow(value);
			}
		}

		public void RegisterOutgoingFlow(OutgoingFlow outgoingMessage)
		{
			var item = new Models.OutgoingFlow() {
				PacketId = outgoingMessage.PacketId,
				Retain = outgoingMessage.Retain,
				Topic = outgoingMessage.Topic,
				Qos = outgoingMessage.Qos,
				Payload = outgoingMessage.Payload,
                Received = outgoingMessage.Received
			};
			conn.InsertOrReplace(item);
		}

		public IEnumerable<OutgoingFlow> GetPendingOutgoingFlows()
		{
			var result = conn.Table<Models.OutgoingFlow>().OrderBy(item => item.Id).Take(1);
			return result.ToList();
		}

		public void SetOutgoingFlowReceived(ushort packetId)
		{
			conn.Execute($@"
                UPDATE {nameof(Models.OutgoingFlow)}
                   SET {nameof(Models.OutgoingFlow.Received)} = 1
			  	WHERE {nameof(Models.OutgoingFlow.PacketId)} = ?
				  AND {nameof(Models.OutgoingFlow.Received)} = 0", packetId);
		}

		public void SetOutgoingFlowCompleted(ushort packetId)
		{
			conn.Execute($"DELETE FROM {nameof(Models.OutgoingFlow)} WHERE {nameof(Models.OutgoingFlow.PacketId)} = ?", packetId);
		}

		public long OutgoingFlowsCount
		{
			get
			{
				return conn.Table<Models.OutgoingFlow>().Count();
			}
		}

		public void Clear()
		{
            try 
			{
				conn.BeginTransaction();
				conn.DeleteAll<Models.IncomingFlow>();
				conn.DeleteAll<Models.LastPacketId>();
				conn.DeleteAll<Models.OutgoingFlow>();
				conn.Commit();
			}
            catch(Exception)
            {
				conn.Rollback();
            }
		}

		public void Dispose()
		{
			if (ConnectionIsManaged)
			{
				conn.Close();
				conn.Dispose();
			}
		}
	}
}

